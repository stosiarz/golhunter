//package com.liquidbcn.golhunter.data

//import com.liquidbcn.golhunter.GolHunterApp
//import com.liquidbcn.golhunter.models.Hunt
//import com.liquidbcn.golhunter.models.WelcomePrice
//import com.parse.ParseQuery
//import com.parse.ParseUser

//class PrizeDataManager {

//    fun postWelcomeGift() {
//        val now = System.currentTimeMillis()/1000
//        getFirstWelcomePrice(now.toString())
//    }
//
//    private fun getFirstWelcomePrice(timestamp: String) {
//        val query = ParseQuery<WelcomePrice>("welcomePrice")
//        query.getFirstInBackground { obj, e ->
//            if (e == null && obj != null) {
//                createWelcomeHunt(timestamp, obj)
//                deleteWelcomePrice(obj)
//            }
//        }
//    }
//
//    private fun deleteWelcomePrice(welcomePrice : WelcomePrice) {
//        welcomePrice.deleteInBackground { e ->
//            if (e == null) {
//                //Log.e("test", "welcome price first removed")
//            } else {
//                //Log.e("test", "welcome price remove failed: "+e.toString())
//            }
//        }
//    }
//
//    private fun createWelcomeHunt(timestamp: String, welcomePrice : WelcomePrice) {
//        val user = ParseUser.getCurrentUser()
//        val huntObj = Hunt()
//
//        huntObj.timestamp = timestamp
//        huntObj.status = HuntDataManager.STATUS_COLLECT
//        huntObj.matchName = "RegaloBienvenida_"+user.objectId
//        huntObj.priceExpiration = welcomePrice.expirationTimestamp
//        huntObj.priceValue = welcomePrice.value
//        huntObj.priceCode = welcomePrice.code
//
//        val relation = huntObj.getRelation<ParseUser>("user")
//        relation.add(user)
//
//        huntObj.saveInBackground { e ->
//            if(e==null) {
//                GolHunterApp.applicationContext().loadData()
//            }
//        }
//    }
//}