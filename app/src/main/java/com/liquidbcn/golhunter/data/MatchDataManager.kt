package com.liquidbcn.golhunter.data

import android.content.Context
import android.content.SharedPreferences
import android.text.TextUtils
import com.liquidbcn.golhunter.models.Match
import com.liquidbcn.golhunter.models.MatchDataDisplay
import com.parse.ParseQuery
import java.util.*

class MatchDataManager(context: Context) {

    val PREFS_FILENAME = "com.liquidbcn.golhunter.prefs"
    var prefs : SharedPreferences? = null

    val observed: ArrayList<MatchDataChanged> = ArrayList()
    val dataset: ArrayList<MatchDataDisplay> = ArrayList()
    var datasetSize: Int = 0

    init {
        prefs = context.getSharedPreferences(PREFS_FILENAME, 0)
        loadGamesFromNetwork()
    }

    fun subscribe(listener: MatchDataChanged) {
        observed.add(listener)
    }

    fun unsubscribe(listener: MatchDataChanged) {
        observed.remove(listener)
    }

    fun clearObservers() {
        observed.clear()
    }

    private fun loadGamesFromNetwork() {
        datasetSize = 0
        val query = ParseQuery<Match>("Match")
        query.orderByAscending("startTimestamp")
        query.findInBackground { objects, e ->
            if(e == null) {
                dataset.clear()
                parseMatches(objects)
                sortMatches()
                notifyObserved()
            }
        }
    }

    private fun parseMatches(matches : List<Match>?) {
        if(matches != null && matches.isNotEmpty()) {
            for(match : Match in matches) {
                val id = match.objectId
                val isProgrammedId = prefs!!.getString(id, null)
                val isProgrammed = isProgrammedId != null
                val isLive = isLive(match.startTimestamp, match.finishTimestamp)

                val data = MatchDataDisplay(id,
                        match.team1Name,
                        match.team2Name,
                        match.matchName,
                        match.status,
                        match.team1Goals.toString(),
                        match.team2Goals.toString(),
                        match.statusGoals,
                        match.startTimestamp,
                        match.finishTimestamp,
                        isProgrammed,
                        isLive)

                if(!isFinished(match.finishTimestamp)) {
                    dataset.add(data)
                    datasetSize++
                }
            }
        }
    }

    private fun sortMatches() {
        dataset.sort()

        val sortedLive: ArrayList<MatchDataDisplay> = ArrayList()
        val sortedRest: ArrayList<MatchDataDisplay> = ArrayList()
        for(match : MatchDataDisplay in dataset) {
            if(match.isLive!!) {
                sortedLive.add(match)
            }
        }

        dataset.removeAll(sortedLive)

        for(match : MatchDataDisplay in dataset) {
            sortedRest.add(match)
        }

        dataset.clear()
        dataset.addAll(sortedLive)
        dataset.addAll(sortedRest)
    }

    private fun isFinished(matchEnd : String?) : Boolean {
        if(!TextUtils.isEmpty(matchEnd)) {
            var now = System.currentTimeMillis()
            val end = matchEnd!!.toLong()*1000
            if(now > end) {
                return true
            }
        }
        return false
    }

    private fun isLive(matchStart : String?, matchEnd : String?) : Boolean {
        if(matchStart!=null) {
            var matchLenghtDefaultMilliseconds = 220*60*1000
            var now = System.currentTimeMillis()

            var start = matchStart.toLong()*1000
            var end = start+matchLenghtDefaultMilliseconds // start date plus 90 minutes gives default end date

            if (!TextUtils.isEmpty(matchEnd)) {
                end = matchEnd!!.toLong()*1000
            }

            if (now in start..(end - 1)) {
                return true
            }
        }
        return false
    }

    private fun notifyObserved() {
        for(toNotify: MatchDataChanged in observed) {
            toNotify.onDataChanged()
        }
    }

    interface MatchDataChanged {
        fun onDataChanged()
    }
}