package com.liquidbcn.golhunter.data

import com.liquidbcn.golhunter.models.Hunt
import com.parse.ParseQuery
import com.parse.ParseUser
import java.util.*

class HuntDataManager {

    companion object {
        var STATUS_PENDING : String = "PENDING"
        var STATUS_FAILED : String = "FAILED"
        var STATUS_COLLECT : String = "SCORED"
    }

    val observed: ArrayList<HuntDataChanged> = ArrayList()
    val dataset: ArrayList<Hunt> = ArrayList()
    var huntSuccess: Int = 0

    init {
        loadHuntsFromNetwork()
    }

    fun subscribe(listener: HuntDataChanged) {
        observed.add(listener)
    }

    fun unsubscribe(listener: HuntDataChanged) {
        observed.remove(listener)
    }

    fun clearObservers() {
        observed.clear()
    }

    private fun loadHuntsFromNetwork() {
        val currentUser = ParseUser.getCurrentUser()

        val query = ParseQuery<Hunt>("Hunt")
        query.whereEqualTo("user", currentUser)

        query.findInBackground { objects, e ->
            if (e == null) {
                dataset.clear()
                dataset.addAll(objects)
                dataset.sort()
                for(hunt:Hunt in objects) {
                    if(hunt.status.equals(STATUS_COLLECT)) {
                        huntSuccess++
                    }
                }
            }

            notifyObserved()
        }
    }

    private fun notifyObserved() {
        for(toNotify: HuntDataChanged in observed) {
            toNotify.onHuntDataChanged()
        }
    }

    interface HuntDataChanged {
        fun onHuntDataChanged()
    }
}