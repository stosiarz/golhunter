package com.liquidbcn.golhunter

import android.app.Application
import android.text.format.DateFormat
import com.liquidbcn.golhunter.data.HuntDataManager
import com.liquidbcn.golhunter.data.MatchDataManager
import com.liquidbcn.golhunter.models.*
import com.parse.*
import java.util.*

class GolHunterApp : Application() {

    lateinit var matchDataManager: MatchDataManager
    lateinit var huntDataManager: HuntDataManager
    //lateinit var prizeDataManager: PrizeDataManager


    init {
        instance = this
    }

    companion object {
        private var instance: GolHunterApp? = null

        fun applicationContext(): GolHunterApp {
            return instance!!
        }

        fun timestampToDate(startMatch: Long?): String {
            if (startMatch != null) {
                val cal = Calendar.getInstance(Locale.ENGLISH)
                cal.timeInMillis = startMatch * 1000
                return DateFormat.format("dd/MM - HH:mm", cal).toString()
            }
            return ""
        }

        fun timestampToDateLong(date: Long?): String {
            if (date != null) {
                val cal = Calendar.getInstance(Locale.ENGLISH)
                cal.timeInMillis = date*1000
                return DateFormat.format("dd/MM/yyyy", cal).toString()
            }
            return ""
        }
    }

    override fun onCreate() {
        super.onCreate()
        //prizeDataManager = PrizeDataManager()

        ParseObject.registerSubclass(Team::class.java)
        ParseObject.registerSubclass(Goal::class.java)
        ParseObject.registerSubclass(Match::class.java)
        ParseObject.registerSubclass(Hunt::class.java)

        Parse.setLogLevel(Parse.LOG_LEVEL_DEBUG)
        //Parse.enableLocalDatastore(this)

        Parse.initialize(Parse.Configuration.Builder(this)
                .applicationId("NP8fzyCrSwA3sb6ew35s0naLUPNMLlMXiSELSUY9")
                .clientKey("25MoSZouSOnUJIM3EwUnNw7fQPXUqlMS4iXEkLNS")
                .server("https://pg-app-c07so4hpgxp0qpf7y5ys2vfgwjiif7.scalabl.cloud/1/")
                //.enableLocalDataStore()
                .build())

        ParseInstallation.getCurrentInstallation().saveInBackground()
        ParseFacebookUtils.initialize(this)

        loadData()
    }

    fun loadData() {
        if (ParseUser.getCurrentUser() != null) {
            matchDataManager = MatchDataManager(applicationContext)
            huntDataManager = HuntDataManager()
        }
    }
}