package com.liquidbcn.golhunter.models

import com.parse.ParseClassName
import com.parse.ParseObject
import com.parse.ParseRelation

@ParseClassName("Match")
class Match: ParseObject() {
    var team1Name by ParseDelegate<String?>()
    var team2Name by ParseDelegate<String?>()
    var matchName by ParseDelegate<String?>()
    var status by ParseDelegate<String?>()
    var statusGoals by ParseDelegate<String?>()
    var team1Goals by ParseDelegate<Int?>()
    var team2Goals by ParseDelegate<Int?>()
    var startTimestamp by ParseDelegate<String?>()
    var finishTimestamp by ParseDelegate<String?>()
}