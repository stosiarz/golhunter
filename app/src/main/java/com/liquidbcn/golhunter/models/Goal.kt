package com.liquidbcn.golhunter.models

import com.parse.ParseClassName
import com.parse.ParseObject

@ParseClassName("Goal")
class Goal: ParseObject() {
    var match by ParseDelegate<Match?>()
    var playerName by ParseDelegate<String?>()
    var team1Goals by ParseDelegate<Int?>()
    var team2Goals by ParseDelegate<Int?>()
    var timestamp by ParseDelegate<String?>()
    var priceURL by ParseDelegate<String?>()
}