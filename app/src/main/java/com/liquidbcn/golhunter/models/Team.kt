package com.liquidbcn.golhunter.models

import com.parse.ParseClassName
import com.parse.ParseObject

@ParseClassName("Team")
class Team: ParseObject() {

    var teamName by ParseDelegate<String?>()
}