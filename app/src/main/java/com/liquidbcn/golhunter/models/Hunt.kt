package com.liquidbcn.golhunter.models

import com.parse.ParseClassName
import com.parse.ParseObject
import com.parse.ParseRelation
import com.parse.ParseUser

@ParseClassName("Hunt")
class Hunt() : ParseObject(), Comparable<Hunt> {
    var user by ParseDelegate<ParseRelation<ParseUser>?>()
    var match by ParseDelegate<ParseRelation<Match>?>()
    var status by ParseDelegate<String?>()
    var timestamp by ParseDelegate<String?>()
    var matchName by ParseDelegate<String?>()
    var userEmail by ParseDelegate<String?>()
    var priceValue by ParseDelegate<Int?>()
    var priceCode by ParseDelegate<String?>()
    var priceExpiration by ParseDelegate<String?>()

    override fun compareTo(other: Hunt): Int {
        if (timestamp != null && other.timestamp != null) {
            return when {
                timestamp!!.toLong() < other.timestamp!!.toLong() -> 1
                timestamp!!.toLong() > other.timestamp!!.toLong() -> -1
                else -> 0
            }
        }
        return 0
    }
}