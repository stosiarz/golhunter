package com.liquidbcn.golhunter.models

data class MatchDataDisplay(var id : String?,
                            var team1: String?,
                            var team2: String?,
                            var matchName: String?,
                            val status: String?,
                            val team1Goals : String?,
                            val team2Goals : String?,
                            val statusGoals : String?,
                            val startTimestamp : String?,
                            val finishTimestamp : String?,
                            var isProgrammed : Boolean?,
                            var isLive : Boolean?) : Comparable<MatchDataDisplay> {

    override fun compareTo(other: MatchDataDisplay) = when {
        startTimestamp!!.toLong() < other.startTimestamp!!.toLong() -> 1
        startTimestamp.toLong() > other.startTimestamp.toLong() -> -1
        else -> 0
    }
}