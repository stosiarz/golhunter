package com.liquidbcn.golhunter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.liquidbcn.golhunter.activities.DashboardActivity
import com.parse.ParseUser

class LoginCompanion {

    companion object {
        fun loginWithUsername(context: Context, activity: Activity, username: String, password: String) {
            ParseUser.logInInBackground(username, password, { user, _ ->
                if (user != null) {
                    openDashboardScreen(context, activity)
                } else {
                    Toast.makeText(context, context.getText(R.string.error_login_failed), Toast.LENGTH_SHORT).show()
                }
            })
        }

        private fun openDashboardScreen(context: Context, activity: Activity) {
            GolHunterApp.applicationContext().loadData()
            activity.finish()
            var intent = Intent(context, DashboardActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            activity.startActivity(intent)
        }
    }
}