package com.liquidbcn.golhunter.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.app.NotificationManager
import android.app.PendingIntent
import android.graphics.Color
import android.support.v4.app.NotificationCompat
import android.support.v4.app.TaskStackBuilder
import com.liquidbcn.golhunter.R
import com.liquidbcn.golhunter.activities.HomeActivity


class GameReminderReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val resultIntent = Intent(context, HomeActivity::class.java)
        val stackBuilder = TaskStackBuilder.create(context!!)
        stackBuilder.addParentStack(HomeActivity::class.java)
        stackBuilder.addNextIntent(resultIntent)
        val resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)

        val pattern = longArrayOf(100, 200, 300, 400)
        val notifcationBuilder = NotificationCompat.Builder(context!!, "GameReminderNotification")
        notifcationBuilder.setAutoCancel(true)
        notifcationBuilder.setLights(Color.BLUE, 500, 500)
        notifcationBuilder.setVibrate(pattern)
        notifcationBuilder.setStyle(NotificationCompat.InboxStyle())
        notifcationBuilder.setSmallIcon(R.drawable.trofeo)
        notifcationBuilder.setContentTitle(context.getText(R.string.app_name))
        notifcationBuilder.setContentText(context.getText(R.string.notification_reminder_desc))
        notifcationBuilder.setContentIntent(resultPendingIntent)

        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(0, notifcationBuilder.build())
    }
}