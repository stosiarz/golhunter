package com.liquidbcn.golhunter.adapters

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.liquidbcn.golhunter.GolHunterApp.Companion.timestampToDate
import com.liquidbcn.golhunter.R
import com.liquidbcn.golhunter.data.HuntDataManager
import com.liquidbcn.golhunter.models.Hunt
import kotlinx.android.synthetic.main.list_game_dark.view.*


class GoalhuntsAdapter(private val myDataset: ArrayList<Hunt>, private val listener: GolHuntsInterface, private val context : Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val EURO = "\u20ac"
        const val TYPE_WELCOME = 0
        const val TYPE_HUNT = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if(viewType == TYPE_WELCOME) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.welcome_premio_view, parent, false)
            return ViewHolderWelcome(view, context, listener)
        }

        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_game_dark, parent, false)
        return ViewHolder(view, context, listener)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as UpdateViewHolder).bindViews(myDataset[position])
    }

    override fun getItemCount() = myDataset.size

    override fun getItemViewType(position: Int): Int {
        //val type = when (position) {
        //    itemCount-1 -> TYPE_WELCOME
        //    else -> TYPE_HUNT
        //}
        return TYPE_HUNT
    }

    class ViewHolderWelcome(view: View, val context: Context, private val listener: GolHuntsInterface) : RecyclerView.ViewHolder(view), UpdateViewHolder {
        val huntGameName = view.hunt_game_name_txt
        val background = view.hunt_card_view
        val huntImg = view.hunt_football_img
        val successButton = view.hunt_status_success_button

        override fun bindViews(data: Hunt) {
            styleStatusCollect()
            successButton.setOnClickListener { listener.onWelcomePremioButtonClick(data) }
        }

        private fun styleStatusCollect() {
            huntGameName.setTextColor(ContextCompat.getColor(context, R.color.colorHuntSuccess))
            background.setBackgroundColor(ContextCompat.getColor(context, R.color.colorWhite))
            huntImg.setImageResource(R.drawable.ball)
            successButton.visibility = VISIBLE
        }
    }

    class ViewHolder(view: View, val context: Context, private val listener: GolHuntsInterface) : RecyclerView.ViewHolder(view), UpdateViewHolder {
        val huntStatus = view.hunt_status
        val huntGameName = view.hunt_game_name_txt
        val huntGameStatus = view.hunt_game_status_txt
        val background = view.hunt_card_view
        val huntImg = view.hunt_football_img
        val successButton = view.hunt_status_success_button

        override fun bindViews(data: Hunt) {
            var status = data.status
            if(status!=null) {
                var statusUpper = status.toUpperCase()
                when (statusUpper) {
                    HuntDataManager.STATUS_PENDING -> styleStatusPending()
                    HuntDataManager.STATUS_FAILED -> styleStatusFailed()
                    HuntDataManager.STATUS_COLLECT -> styleStatusCollect()
                }

                val matchRelation = data.match
                if(matchRelation!=null) {
                    matchRelation.query?.getFirstInBackground { obj, e ->
                        if (obj != null) {
                            val name1 = obj.team1Name
                            val name2 = obj.team2Name
                            val name = "$name1 - $name2"
                            huntGameName.text = name
                        }
                    }
                }

                val timestamp = data.timestamp
                if (timestamp != null) {
                    huntGameStatus.text = timestampToDate(timestamp.toLong())
                }

                successButton.setOnClickListener { listener.onPremioButtonClick(data) }
            }
        }

        private fun styleStatusPending() {
            huntStatus.setTextColor(ContextCompat.getColor(context, R.color.colorBackgroundBtnGray))
            huntGameName.setTextColor(ContextCompat.getColor(context, R.color.colorWhite))
            huntGameStatus.setTextColor(ContextCompat.getColor(context, R.color.colorWhite))
            background.setBackgroundColor(ContextCompat.getColor(context, R.color.colorBackgroundListLightGray))
            huntImg.setImageResource(R.drawable.ballwhite)
            huntStatus.text = context.getText(R.string.goalhunt_button_pendiente)
            successButton.visibility = GONE
            huntStatus.visibility = VISIBLE
        }

        private fun styleStatusFailed() {
            huntStatus.setTextColor(ContextCompat.getColor(context, R.color.colorBackgroundBtnPink))
            huntGameName.setTextColor(ContextCompat.getColor(context, R.color.colorWhite))
            huntGameStatus.setTextColor(ContextCompat.getColor(context, R.color.colorWhite))
            background.setBackgroundColor(ContextCompat.getColor(context, R.color.colorBackgroundListLightGray))
            huntImg.setImageResource(R.drawable.ballwhite)
            huntStatus.text = context.getText(R.string.goalhunt_button_sin_goles)
            successButton.visibility = GONE
            huntStatus.visibility = VISIBLE
        }

        private fun styleStatusCollect() {
            huntStatus.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
            huntGameName.setTextColor(ContextCompat.getColor(context, R.color.colorHuntSuccess))
            huntGameStatus.setTextColor(ContextCompat.getColor(context, R.color.colorHuntSuccess))
            background.setBackgroundColor(ContextCompat.getColor(context, R.color.colorWhite))
            huntImg.setImageResource(R.drawable.ball)
            huntStatus.text = context.getText(R.string.goalhunt_button_ver_premio)
            successButton.visibility = VISIBLE
            huntStatus.visibility = GONE
        }
    }

    interface UpdateViewHolder {
        fun bindViews(data: Hunt)
    }

    interface GolHuntsInterface {
        fun onPremioButtonClick(data: Hunt)
        fun onWelcomePremioButtonClick(data: Hunt)
    }
}