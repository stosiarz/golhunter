package com.liquidbcn.golhunter.adapters

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.liquidbcn.golhunter.GolHunterApp
import com.liquidbcn.golhunter.R
import com.liquidbcn.golhunter.models.MatchDataDisplay
import kotlinx.android.synthetic.main.list_game.view.*
import java.util.*

class GamesAdapter(private val myDataset: ArrayList<MatchDataDisplay>, private val context: Context, private val listener: DashboardInterface) : RecyclerView.Adapter<GamesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GamesAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_game, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var data = myDataset[position]
        var players = data.team1+" - "+data.team2
        var name = data.matchName
        if(TextUtils.isEmpty(name)) {
            name = ""
        }

        holder.gameName.text = players
        holder.gameBtn.setOnClickListener { handleButtonClick(data.id, data.startTimestamp, data.isLive, data.isProgrammed, players, name!!) }
        defineViewByType(holder, data)
    }

    override fun getItemCount() = myDataset.size

    private fun defineViewByType(holder: ViewHolder, data : MatchDataDisplay) {
        var startDate = 0L
        if(data.startTimestamp!=null) {
            startDate = data.startTimestamp.toLong()
        }

        if(data.isLive!!) {
            holder.gameStatus.text = context.getString(R.string.game_list_status)
            holder.gameStatus.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark))
            holder.gameBtn.setBackgroundColor(ContextCompat.getColor(context, R.color.matchActionButtonInGame))
            holder.gameBtn.text = context.getString(R.string.game_list_button_status_cezar_goles)
        } else if(data.isProgrammed!!){
            holder.gameStatus.text = GolHunterApp.timestampToDate(startDate)
            holder.gameStatus.setTextColor(ContextCompat.getColor(context, R.color.colorDashboardStatusTextDate))
            holder.gameBtn.setBackgroundColor(ContextCompat.getColor(context, R.color.matchActionButtonProgrammed))
            holder.gameBtn.text = context.getString(R.string.game_list_button_status_programado)
        } else {
            holder.gameStatus.text = GolHunterApp.timestampToDate(startDate)
            holder.gameStatus.setTextColor(ContextCompat.getColor(context, R.color.colorDashboardStatusTextDate))
            holder.gameBtn.setBackgroundColor(ContextCompat.getColor(context, R.color.matchActionButtonPending))
            holder.gameBtn.text = context.getString(R.string.game_list_button_status_crear_aviso)
        }
    }

    private fun handleButtonClick(id : String?, start : String?, isLive : Boolean?, isProgrammed : Boolean?, players: String, matchName : String) {
        if(isLive!!) {
            listener.onCaptureGoalClick(id!!, players, matchName)
        } else if(!isProgrammed!!) {
            listener.programMatch(id, start)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val gameName = view.game_name_txt
        val gameStatus = view.game_status_txt
        val gameBtn = view.game_action_btn
    }

    interface DashboardInterface {
        fun onCaptureGoalClick(matchId: String, players: String, matchName : String)
        fun programMatch(matchId : String?, matchStart : String?)
    }
}