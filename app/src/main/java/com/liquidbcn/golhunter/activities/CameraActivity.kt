package com.liquidbcn.golhunter.activities

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.SurfaceTexture
import android.hardware.Camera
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.TextureView
import android.widget.Toast
import com.liquidbcn.golhunter.R
import kotlinx.android.synthetic.main.activity_camera.*
import java.io.IOException
import android.graphics.Matrix
import android.os.CountDownTimer
import android.view.Surface
import android.view.View.GONE
import android.view.View.VISIBLE
import com.liquidbcn.golhunter.GolHunterApp
import com.liquidbcn.golhunter.data.HuntDataManager
import com.liquidbcn.golhunter.models.Hunt
import com.liquidbcn.golhunter.models.Match
import com.parse.ParseQuery
import com.parse.ParseUser

class CameraActivity : AppCompatActivity(), TextureView.SurfaceTextureListener {

    private val CAMERA_REQUEST_CODE = 1
    private var camera: Camera? = null
    private var orgPreviewWidth: Int = 1280
    private var orgPreviewHeight: Int = 720
    private var timerCount: Int = 10
    private var isRunning: Boolean = false
    private var timer: CountDownTimer? = null
    private var match : Match? = null
    private var matchName : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera)
        val matchId = intent.extras.getString("matchId", "")
        matchName = intent.extras.getString("matchName", "")

        getMatchById(matchId)
        camera_match_name.text = intent.extras.getString("players", "")
        camera_exit_btn.setOnClickListener { finish() }
        camera_capture_btn.setOnClickListener { handleButtonClick() }
    }

    private fun handleButtonClick() {
        if(!isRunning) {
            startCapture()
        } else {
            onCountdownFinish()
        }
    }
    override fun onResume() {
        super.onResume()
        setupPermissions()
    }

    override fun onStop() {
        super.onStop()
        releaseCameraAndPreview()
        stopTimer()
        finish()
    }

    private fun stopTimer() {
        if (timer != null) {
            timer!!.cancel()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        stopCapture()
    }

    private fun startCapture() {
        if(!isRunning) {
            isRunning = true

            timer = object : CountDownTimer(10 * 1000, 1000) {
                override fun onFinish() = onCountdownFinish()

                override fun onTick(millisUntilFinished: Long) {
                    timeTick()
                }
            }.start()

            camera_capture_btn_text.text = getString(R.string.camera_button_finish)
            camera_capture_btn_img.visibility = GONE

            camera_timer.text = timeToString()
            camera_timer.visibility = VISIBLE

            //send data immediately
            val timestamp = System.currentTimeMillis()/1000
            postHunt(timestamp.toString())
        }
    }

    private fun stopCapture() {
        if(isRunning) {
            stopTimer()
            isRunning = false
        }
    }

    private fun postHunt(timestamp: String) {
        val user = ParseUser.getCurrentUser()
        val huntObj = Hunt()

        huntObj.timestamp = timestamp
        huntObj.status = HuntDataManager.STATUS_PENDING
        huntObj.matchName = matchName
        huntObj.userEmail = user.email
        val relation = huntObj.getRelation<ParseUser>("user")
        relation.add(user)

        if(match!=null) {
            val relationMatch = huntObj.getRelation<Match>("match")
            relationMatch.add(match)
        }

        huntObj.saveInBackground { e ->
            if(e==null) {
                GolHunterApp.applicationContext().loadData()
            }
        }
    }

    private fun getMatchById(id: String) {
        val query = ParseQuery<Match>("Match")
        query.whereEqualTo("objectId", id)
        query.getFirstInBackground { matchObject, e ->
            match = matchObject
        }
    }

    private fun timeToString() : String {
        var time = ""
        var seconds = Integer.toString(timerCount)
        if(timerCount>=10) {
            time = "00:$seconds"
        } else {
            time = "00:0$seconds"
        }
        return time
    }

    private fun timeTick() {
        if(isRunning) {
            timerCount -= 1
            camera_timer.text = timeToString()
        }
    }

    private fun onCountdownFinish() {
        if(timer!=null) {
            timer!!.cancel()
        }

        isRunning = false
        timerCount -= 1
        camera_timer.text = timeToString()
        var intent = Intent(applicationContext, GoalCapturedActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun setupPermissions() {
        val permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            makeRequest()
        } else {
            attachSurfaceListner()
        }
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.CAMERA),
                CAMERA_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            CAMERA_REQUEST_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(applicationContext, "Permissions not granted", Toast.LENGTH_SHORT).show()
                    finish()
                } else {
                    attachSurfaceListner()
                    setupAndStartCamera(camera_surface.surfaceTexture)
                }
            }
        }
    }

    private fun attachSurfaceListner() {
        camera_surface.surfaceTextureListener = this
    }

    private fun releaseCameraAndPreview() {
        if (camera != null) {
            camera!!.release()
            camera = null
        }
    }

    private fun setupAndStartCamera(surface: SurfaceTexture?) {
        camera = Camera.open()
        if(camera != null) {
            try {
                val parameters = camera!!.parameters
                parameters.focusMode = Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO

                val size = getMaxSize(parameters.supportedPreviewSizes)
                parameters.setPreviewSize(size.first, size.second)

                orgPreviewWidth = size.first
                orgPreviewHeight = size.second

                camera!!.parameters = parameters

                //updateTextureMatrix(width, height)
                setCameraDisplayOrientation()
                camera!!.setPreviewTexture(surface)
                camera!!.startPreview()
            } catch (ioe: IOException) {
            }
        }
    }

    override fun onSurfaceTextureSizeChanged(surface: SurfaceTexture?, width: Int, height: Int) {
        //updateTextureMatrix(width, height)
    }

    override fun onSurfaceTextureAvailable(surface: SurfaceTexture?, width: Int, height: Int) {
        setupAndStartCamera(surface)
    }

    override fun onSurfaceTextureDestroyed(surface: SurfaceTexture?): Boolean {
        releaseCameraAndPreview()
        return true
    }

    override fun onSurfaceTextureUpdated(surface: SurfaceTexture?) {
    }

    private fun getMaxSize(list: List<Camera.Size>): Pair<Int, Int> {
        var width = 0
        var height = 0

        for (size in list) {
            if (size.width * size.height > width * height) {
                width = size.width
                height = size.height
            }
        }

        return Pair(width, height)
    }

    private fun updateTextureMatrix(width: Int, height: Int) {
        var isPortrait = false

        val display = windowManager.defaultDisplay
        if (display.rotation == Surface.ROTATION_0 || display.rotation == Surface.ROTATION_180)
            isPortrait = true
        else if (display.rotation == Surface.ROTATION_90 || display.rotation == Surface.ROTATION_270) isPortrait = false

        var previewWidth = orgPreviewWidth
        var previewHeight = orgPreviewHeight

        if (isPortrait) {
            previewWidth = orgPreviewHeight
            previewHeight = orgPreviewWidth
        }

        val ratioSurface = width.toFloat() / height
        val ratioPreview = previewWidth.toFloat() / previewHeight

        val scaleX: Float
        val scaleY: Float

        if (ratioSurface > ratioPreview) {
            scaleX = height.toFloat() / previewHeight
            scaleY = 1f
        } else {
            scaleX = 1f
            scaleY = width.toFloat() / previewWidth
        }

        val matrix = Matrix()

        matrix.setScale(scaleX, scaleY)
        camera_surface.setTransform(matrix)

        val scaledWidth = width * scaleX
        val scaledHeight = height * scaleY

        val dx = (width - scaledWidth) / 2
        val dy = (height - scaledHeight) / 2
        camera_surface.translationX = dx
        camera_surface.translationY = dy
    }

    fun setCameraDisplayOrientation() {
        if(camera!=null) {
            val info = Camera.CameraInfo()
            Camera.getCameraInfo(0, info)
            val rotation = this.windowManager.defaultDisplay.rotation
            var degrees = 0
            when (rotation) {
                Surface.ROTATION_0 -> degrees = 0
                Surface.ROTATION_90 -> degrees = 90
                Surface.ROTATION_180 -> degrees = 180
                Surface.ROTATION_270 -> degrees = 270
            }

            var result: Int
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                result = (info.orientation + degrees) % 360
                result = (360 - result) % 360  // compensate the mirror
            } else {  // back-facing
                result = (info.orientation - degrees + 360) % 360
            }
            camera!!.setDisplayOrientation(result)

            val params = camera!!.parameters
            params.setRotation(result)
            camera!!.parameters = params
        }
    }
}