package com.liquidbcn.golhunter.activities

import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.liquidbcn.golhunter.R
import kotlinx.android.synthetic.main.activity_home.*
import com.liquidbcn.golhunter.GolHunterApp
import com.parse.*


class HomeActivity : AppCompatActivity() {

    val permissions: List<String> = object : ArrayList<String>() {
        init {
            add("public_profile")
            add("email")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isUserLoggedIn()
        setContentView(R.layout.activity_home)
        home_login_email.paintFlags = Paint.UNDERLINE_TEXT_FLAG
        home_login_email.setOnClickListener { openLoginEmail() }
        login_button.setOnClickListener {  fbLogin() }
    }

    private fun fbLogin() {
        ParseFacebookUtils.logInWithReadPermissionsInBackground(this, permissions, { user, e ->
            if(e==null) {
                if (user == null) {
                    //Log.e("test", "Uh oh. The user cancelled the Facebook login. "+e.toString())
                } else if (user.isNew) {
                    openLoginFacebook()
                } else {
                    val currentUser = ParseUser.getCurrentUser()
                    if (currentUser != null) {
                        GolHunterApp.applicationContext().loadData()
                        openDashboard()
                    } else {
                        openLoginFacebook()
                    }
                }
            } else {
                Toast.makeText(applicationContext, e.localizedMessage, Toast.LENGTH_SHORT).show()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data)
    }

    private fun openDashboard() {
        finish()
        var intent = Intent(applicationContext, DashboardActivity::class.java)
        startActivity(intent)
    }

    private fun openLoginEmail() {
        var intent = Intent(applicationContext, RegisterEmailActivity::class.java)
        startActivity(intent)
    }

    private fun openLoginFacebook() {
        var intent = Intent(applicationContext, LoginFacebookActivity::class.java)
        startActivity(intent)
    }

    private fun isUserLoggedIn() {
        val currentUser = ParseUser.getCurrentUser()
        if (currentUser != null) {
            openDashboard()
        }
    }
}