package com.liquidbcn.golhunter.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View.GONE
import android.view.View.VISIBLE
import com.liquidbcn.golhunter.GolHunterApp
import com.liquidbcn.golhunter.R
import com.liquidbcn.golhunter.adapters.GoalhuntsAdapter
import com.liquidbcn.golhunter.data.HuntDataManager
import com.liquidbcn.golhunter.models.Hunt
import com.parse.ParseUser
import kotlinx.android.synthetic.main.activity_goalhunts.*
import kotlinx.android.synthetic.main.menu_custom.*

class GoalhuntsActivity : AppCompatActivity(), HuntDataManager.HuntDataChanged, GoalhuntsAdapter.GolHuntsInterface {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    private var dataset: ArrayList<Hunt> = ArrayList()
    private var isMenuOpen: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_goalhunts)

        dataset = GolHunterApp.applicationContext().huntDataManager.dataset
        viewManager = LinearLayoutManager(this)
        viewAdapter = GoalhuntsAdapter(dataset, this, applicationContext)

        recyclerView = findViewById<RecyclerView>(R.id.goalhunts_recyclerview).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

        goalhunt_tools_holder.setOnClickListener { openMenu() }
        menu_custom_close_button.setOnClickListener { openMenu() }
        menu_custom_one.setOnClickListener { closeSession() }
        menu_custom_two.setOnClickListener { openPrivacyPolicy() }
        menu_custom_three.setOnClickListener { openUseConditions() }
        menu_custom_four.setOnClickListener { openContact() }
    }

    override fun onResume() {
        super.onResume()
        GolHunterApp.applicationContext().huntDataManager.subscribe(this)
    }

    override fun onStop() {
        super.onStop()
        GolHunterApp.applicationContext().huntDataManager.unsubscribe(this)
    }

    fun openMenu() {
        if(!isMenuOpen) {
            isMenuOpen = true
            menu_custom_main.visibility = VISIBLE
        } else {
            isMenuOpen = false
            menu_custom_main.visibility = GONE
        }
    }

    private fun closeSession() {
        openMenu()
        ParseUser.logOutInBackground { logoutAndReopenHome() }
    }

    private fun openPrivacyPolicy() {
        openMenu()
        openUrl(getString(R.string.url_privacy))
    }

    private fun openUseConditions() {
        openMenu()
        openUrl(getString(R.string.url_terms_of_use))
    }

    private fun openContact() {
        openMenu()
        val intent = Intent(Intent.ACTION_SENDTO)
        intent.data = Uri.parse("mailto:")
        intent.putExtra(Intent.EXTRA_EMAIL, "")
        intent.putExtra(Intent.EXTRA_SUBJECT, "")
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }

    private fun logoutAndReopenHome() {
        val intent = Intent(applicationContext, HomeActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
    }

    override fun onHuntDataChanged() {
        viewAdapter.notifyDataSetChanged()
    }

    override fun onPremioButtonClick(data: Hunt) {
        val intent = Intent(applicationContext, WinnerActivity::class.java)
        intent.putExtra(WinnerActivity.HUNT_CODE, data.priceCode)
        intent.putExtra(WinnerActivity.HUNT_VALUE, data.priceValue)
        intent.putExtra(WinnerActivity.HUNT_EXPERIATION, data.priceExpiration)
        startActivity(intent)
    }

    override fun onWelcomePremioButtonClick(data: Hunt) {
        val intent = Intent(applicationContext, WinnerActivity::class.java)
        intent.putExtra(WinnerActivity.HUNT_CODE, data.priceCode)
        intent.putExtra(WinnerActivity.HUNT_VALUE, data.priceValue)
        intent.putExtra(WinnerActivity.HUNT_EXPERIATION, data.priceExpiration)
        startActivity(intent)
    }

    private fun openUrl(url: String) {
        val intent = Intent(applicationContext, WebViewActivity::class.java)
        intent.putExtra(WebViewActivity.URL_FIELD, url)
        startActivity(intent)
    }
}