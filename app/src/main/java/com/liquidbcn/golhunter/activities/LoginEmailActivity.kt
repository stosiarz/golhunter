package com.liquidbcn.golhunter.activities

import android.graphics.Paint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.liquidbcn.golhunter.LoginCompanion
import com.liquidbcn.golhunter.R
import com.parse.ParseUser
import kotlinx.android.synthetic.main.activity_login_email.*

class LoginEmailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_email)
        login_email_back_to_register.paintFlags = Paint.UNDERLINE_TEXT_FLAG
        login_email_enter_btn.setOnClickListener { logInAndEnter() }
        login_email_back_to_register.setOnClickListener { finish() }

        val hintEmail = getString(R.string.login_email_hint_email)
        val hintPass = getString(R.string.login_email_hint_password)

        login_email_input_email.editText!!.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            login_email_input_email.isHintEnabled = false
            login_email_input_email.editText!!.hint = hintEmail
        }

        login_email_input_password.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            login_email_input_password.isHintEnabled = false
            login_email_input_password.editText!!.hint = hintPass
        }
    }

    private fun logInAndEnter() {
        var email = login_email_input_email.editText!!.text.toString()
        var password = login_email_input_password.editText!!.text.toString()
        logInUser(email, password)
    }

    private fun logInUser(username: String, password : String) {
        if(username.contains("@")) {
            val query = ParseUser.getQuery().whereEqualTo("email", username)
            query.getFirstInBackground { item, _ ->
                if(item!=null) {
                    LoginCompanion.loginWithUsername(applicationContext, this, item.username, password)
                }
            }
        } else {
            LoginCompanion.loginWithUsername(applicationContext, this, username, password)
        }
    }
}