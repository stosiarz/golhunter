package com.liquidbcn.golhunter.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.webkit.WebView
import com.liquidbcn.golhunter.R
import android.webkit.WebViewClient

class WebViewActivity : AppCompatActivity() {

    companion object {
        const val URL_FIELD = "url"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)

        val url = intent.getStringExtra(URL_FIELD)
        if (url == null || url.isEmpty()) finish()

        val webView = findViewById<WebView>(R.id.webview) as WebView
        webView.settings.javaScriptEnabled = true
        webView.webViewClient = WebViewClient()
        webView.loadUrl(url)
    }
}