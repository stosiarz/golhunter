package com.liquidbcn.golhunter.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.liquidbcn.golhunter.R
import kotlinx.android.synthetic.main.activity_goalcaptured.*

class GoalCapturedActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_goalcaptured)
        goalhunt_completed_btn.setOnClickListener { finish() }
    }
}