package com.liquidbcn.golhunter.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.RelativeLayout
import com.liquidbcn.golhunter.R
import com.liquidbcn.golhunter.adapters.GamesAdapter
import com.liquidbcn.golhunter.models.MatchDataDisplay
import kotlinx.android.synthetic.main.activity_dashboard.*
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.widget.Toast
import com.liquidbcn.golhunter.GolHunterApp
import com.liquidbcn.golhunter.data.HuntDataManager
import com.liquidbcn.golhunter.data.MatchDataManager
import com.liquidbcn.golhunter.receivers.GameReminderReceiver
import com.parse.ParsePush
import java.util.*

class DashboardActivity : AppCompatActivity(), GamesAdapter.DashboardInterface, MatchDataManager.MatchDataChanged, HuntDataManager.HuntDataChanged {

    private val GAME_LIMIT = 40
    private val PUSH_CHANNEL_NAME = "push"
    private val PREFS_FILENAME = "com.liquidbcn.golhunter.prefs"
    private var prefs : SharedPreferences? = null

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    private lateinit var dataset: ArrayList<MatchDataDisplay>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        ParsePush.subscribeInBackground(PUSH_CHANNEL_NAME)
        prefs = this.getSharedPreferences(PREFS_FILENAME, 0)

        dashboard_goal_number.text = "0"

        dataset = GolHunterApp.applicationContext().matchDataManager.dataset

        viewManager = LinearLayoutManager(this)
        viewAdapter = GamesAdapter(dataset, applicationContext, this)

        recyclerView = findViewById<RecyclerView>(R.id.dashboard_main_recyclerview).apply {
            setHasFixedSize(true)
            isNestedScrollingEnabled = false
            layoutManager = viewManager
            adapter = viewAdapter
        }

        var dashboardTropheHolder = findViewById<RelativeLayout>(R.id.dashboard_trophe_holder)
        dashboardTropheHolder.setOnClickListener { openGoalhuntsActivity() }
    }

    override fun onResume() {
        super.onResume()
        GolHunterApp.applicationContext().matchDataManager.subscribe(this)
        GolHunterApp.applicationContext().huntDataManager.subscribe(this)
        GolHunterApp.applicationContext().loadData()
    }

    override fun onStop() {
        super.onStop()
        GolHunterApp.applicationContext().matchDataManager.unsubscribe(this)
        GolHunterApp.applicationContext().huntDataManager.unsubscribe(this)
    }

    private fun openGoalhuntsActivity() {
        val intent = Intent(applicationContext, GoalhuntsActivity::class.java)
        startActivity(intent)
    }

    private fun openCameractivity(matchId: String, players: String, matchName: String) {
        val intent = Intent(applicationContext, CameraActivity::class.java)
        intent.putExtra("matchId", matchId)
        intent.putExtra("players", players)
        intent.putExtra("matchName", matchName)
        startActivity(intent)
    }

    private fun scheduleMatchReminder(matchId : String?, matchStart : String?) {
        if(matchId!=null) {
            val intent = Intent(this, GameReminderReceiver::class.java)
            val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
            var alarmIntent = PendingIntent.getBroadcast(applicationContext, 0, intent, 0)

            var fiveMinutesInMilliseconds = 5 * 60 * 1000
            var start = System.currentTimeMillis()
            if(matchStart != null) {
                start = matchStart!!.toLong()*1000
            }

            var scheduleAt = start - fiveMinutesInMilliseconds

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, scheduleAt, alarmIntent)
            } else {
                alarmManager.set(AlarmManager.RTC_WAKEUP, scheduleAt, alarmIntent)
            }

            saveMatchInSharedPrefs(matchId)
            viewAdapter.notifyDataSetChanged()
        }
    }

    private fun saveMatchInSharedPrefs(matchId: String) {
        val editor = prefs!!.edit()
        editor.putString(matchId, matchId).apply()
        updateUiForProgrammed(matchId)
    }

    private fun updateUiForProgrammed(matchId: String) {
        for(match : MatchDataDisplay in dataset) {
            if(matchId.equals(match.id)) {
                match.isProgrammed = true
            }
        }
    }

    private fun isGameLimit(currentGameState: Int) : Boolean {
        if(currentGameState >= GAME_LIMIT) {
            return true
        }
        return false
    }

    private fun saveGameState(matchId: String, gameState: Int) {
        var newGameState = gameState+1
        val editor = prefs!!.edit()
        editor.putInt("gamestate_$matchId", newGameState).apply()
    }

    private fun loadGameState(matchId: String): Int {
        return prefs!!.getInt("gamestate_$matchId", 0)
    }

    override fun onCaptureGoalClick(matchId: String, players: String, matchName : String) {
        val currentGameState = loadGameState(matchId)
        if(!isGameLimit(currentGameState)) {
            saveGameState(matchId, currentGameState)
            openCameractivity(matchId, players, matchName)
        } else {
            Toast.makeText(applicationContext, getString(R.string.error_game_limit_reached), Toast.LENGTH_SHORT).show()
        }
    }

    override fun programMatch(matchId : String?, matchStart : String?) {
        scheduleMatchReminder(matchId, matchStart)
    }

    override fun onDataChanged() {
        viewAdapter.notifyDataSetChanged()
    }

    override fun onHuntDataChanged() {
        dashboard_goal_number.text = GolHunterApp.applicationContext().huntDataManager.huntSuccess.toString()
    }
}
