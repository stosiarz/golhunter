package com.liquidbcn.golhunter.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import com.liquidbcn.golhunter.GolHunterApp
import com.liquidbcn.golhunter.R
import com.liquidbcn.golhunter.adapters.GoalhuntsAdapter.Companion.EURO
import kotlinx.android.synthetic.main.activity_winner.*

class WinnerActivity : AppCompatActivity() {

    companion object {
        const val HUNT_VALUE = "hunt_value"
        const val HUNT_CODE = "hunt_code"
        const val HUNT_EXPERIATION = "hunt_experiation"
    }

    private var huntValue : Int? =  null
    private var huntCode : String? =  null
    private var huntExperiation : String? =  null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        huntValue = intent.getIntExtra(HUNT_VALUE, 0)
        huntCode = intent.getStringExtra(HUNT_CODE)
        huntExperiation = intent.getStringExtra(HUNT_EXPERIATION)

        if(huntValue!=null && !TextUtils.isEmpty(huntExperiation) && !TextUtils.isEmpty(huntCode)) {
            setContentView(R.layout.activity_winner)
            winner_money_text.text = huntValue.toString()+EURO
            winner_text_code.text = huntCode.toString()
            var date = GolHunterApp.timestampToDateLong(huntExperiation!!.toLong())
            var experationText = winner_text_bottom.text.toString() +" "+ date
            winner_text_bottom.text = experationText
            winner_completed_btn.setOnClickListener { openUrl() }
            winner_close_btn.setOnClickListener { finish() }
        } else {
            finish()
        }
    }

    private fun openUrl() {
        val intent = Intent(applicationContext, WebViewActivity::class.java)
        intent.putExtra(WebViewActivity.URL_FIELD, getString(R.string.url_redeem))
        startActivity(intent)
    }
}