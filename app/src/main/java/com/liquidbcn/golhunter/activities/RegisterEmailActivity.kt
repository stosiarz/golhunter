package com.liquidbcn.golhunter.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.liquidbcn.golhunter.GolHunterApp
import com.liquidbcn.golhunter.LoginCompanion
import com.liquidbcn.golhunter.R
import com.parse.ParseUser
import kotlinx.android.synthetic.main.activity_register_email.*
import android.graphics.Color
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan

class RegisterEmailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_email)
        register_email_login_btn.setOnClickListener { openLoginScreen() }
        register_email_register_btn.setOnClickListener { signUp() }

        val hintName = getString(R.string.login_email_hint_name)
        val hintEmail = getString(R.string.login_email_hint_email)
        val hintProvince = getString(R.string.login_email_hint_province)
        val hintPass = getString(R.string.login_email_hint_password)

        register_email_input_name.editText!!.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            register_email_input_name.isHintEnabled = false
            register_email_input_name.editText!!.hint = hintName
        }

        register_email_input_email.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            register_email_input_email.isHintEnabled = false
            register_email_input_email.editText!!.hint = hintEmail
        }

        register_email_input_province.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            register_email_input_province.isHintEnabled = false
            register_email_input_province.editText!!.hint = hintProvince
        }

        register_email_input_password.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            register_email_input_password.isHintEnabled = false
            register_email_input_password.editText!!.hint = hintPass
        }

        buildCheckboxText()
    }

    private fun buildCheckboxText() {
        val spanUse = SpannableString(getString(R.string.login_email_privacy_checkbox_button_one))
        spanUse.setSpan(object : NoUnderlineSpan() {
            override fun onClick(v: View) {
                openTermsOfUse()
            }
        }, 0, spanUse.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

        val spanPrivacy = SpannableString(getString(R.string.login_email_privacy_checkbox_button_two))
        spanPrivacy.setSpan(object : NoUnderlineSpan() {
            override fun onClick(v: View) {
                openPrivacyConditions()
            }
        }, 0, spanPrivacy.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

        val start = getString(R.string.login_email_privacy_checkbox_start)
        val and = getString(R.string.login_email_privacy_checkbox_and)

        var spanString = SpannableString(TextUtils.concat(start, " ", spanUse, " ",and, " ", spanPrivacy, "."))

        register_email_checkbox_privacy_text.setLinkTextColor(Color.BLACK)
        register_email_checkbox_privacy_text.text = spanString
        register_email_checkbox_privacy_text.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun openPrivacyConditions() {
        val intent = Intent(applicationContext, WebViewActivity::class.java)
        intent.putExtra(WebViewActivity.URL_FIELD, getString(R.string.url_privacy))
        startActivity(intent)
    }

    private fun openTermsOfUse() {
        val intent = Intent(applicationContext, WebViewActivity::class.java)
        intent.putExtra(WebViewActivity.URL_FIELD, getString(R.string.url_terms_of_use))
        startActivity(intent)
    }

    private fun openLoginScreen() {
        var intent = Intent(applicationContext, LoginEmailActivity::class.java)
        startActivity(intent)
    }

    private fun signUp() {
        var username =  register_email_input_name.editText!!.text.toString()
        var password = register_email_input_password.editText!!.text.toString()
        var email = register_email_input_email.editText!!.text.toString()
        var province = register_email_input_province.editText!!.text.toString()
        val conditions = areConditionsAccepted()
        var registerOK = parseRegisterData(username, password, email, province) && conditions

        if(!conditions) {
            Toast.makeText(applicationContext, getString(R.string.error_accept_conditions), Toast.LENGTH_SHORT).show()
        }

        if(registerOK) {
            val user = ParseUser()
            user.username = username
            user.setPassword(password)
            user.email = email
            user.put("province", province)
            user.put("balance", 100)

            user.signUpInBackground { e ->
                if (e == null) {
                    LoginCompanion.loginWithUsername(applicationContext, this, username, password)
                    //GolHunterApp.applicationContext().prizeDataManager.postWelcomeGift()
                } else {
                    Toast.makeText(applicationContext, e.localizedMessage, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun parseRegisterData(name : String, password : String, email : String, province : String) : Boolean {
        val emptyError = getString(R.string.error_register_field_empty)
        var isDataOk = true
        if(TextUtils.isEmpty(name)) {
            register_email_input_name.error = emptyError
            isDataOk = false
        }

        if(TextUtils.isEmpty(password)) {
            register_email_input_password.error = emptyError
            isDataOk = false
        }

        if(TextUtils.isEmpty(email)) {
            register_email_input_email.error = emptyError
            isDataOk = false
        }

        if(TextUtils.isEmpty(province)) {
            register_email_input_province.error = emptyError
            isDataOk = false
        }

        return isDataOk
    }

    private fun areConditionsAccepted() : Boolean {
        return register_email_checkbox_privacy.isChecked && register_email_checkbox_push.isChecked
    }

    abstract inner class NoUnderlineSpan : ClickableSpan() {
        override fun updateDrawState(ds: TextPaint) {
            super.updateDrawState(ds)
            ds.isUnderlineText = false
        }
    }
}