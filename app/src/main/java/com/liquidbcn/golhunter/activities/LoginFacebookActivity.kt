package com.liquidbcn.golhunter.activities

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.SpannableString
import android.text.Spanned
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.facebook.HttpMethod
import com.liquidbcn.golhunter.GolHunterApp
import com.liquidbcn.golhunter.R
import com.parse.ParseUser
import kotlinx.android.synthetic.main.activity_login_facebook.*
import org.json.JSONException
import android.text.TextPaint
import android.widget.Toast


class LoginFacebookActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_facebook)
        login_facebook_enter_btn.setOnClickListener { getUserDetailsFromFB() }
        buildCheckboxText()
    }

    private fun buildCheckboxText() {
        val spanUse = SpannableString(getString(R.string.login_email_privacy_checkbox_button_one))
        spanUse.setSpan(object : NoUnderlineSpan() {
            override fun onClick(v: View) {
                openTermsOfUse()
            }
        }, 0, spanUse.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

        val spanPrivacy = SpannableString(getString(R.string.login_email_privacy_checkbox_button_two))
        spanPrivacy.setSpan(object : NoUnderlineSpan() {
            override fun onClick(v: View) {
                openPrivacyConditions()
            }
        }, 0, spanPrivacy.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

        val start = getString(R.string.login_email_privacy_checkbox_start)
        val and = getString(R.string.login_email_privacy_checkbox_and)

        var spanString = SpannableString(TextUtils.concat(start, " ", spanUse, " ", and, " ", spanPrivacy, "."))

        login_facebook_checkbox_privacy_text.setLinkTextColor(Color.BLACK)
        login_facebook_checkbox_privacy_text.text = spanString
        login_facebook_checkbox_privacy_text.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun openPrivacyConditions() {
        val intent = Intent(applicationContext, WebViewActivity::class.java)
        intent.putExtra(WebViewActivity.URL_FIELD, getString(R.string.url_privacy))
        startActivity(intent)
    }

    private fun openTermsOfUse() {
        val intent = Intent(applicationContext, WebViewActivity::class.java)
        intent.putExtra(WebViewActivity.URL_FIELD, getString(R.string.url_terms_of_use))
        startActivity(intent)
    }

    private fun areConditionsAccepted() : Boolean {
        return login_facebook_checkbox_privacy.isChecked && login_facebook_checkbox_push.isChecked
    }

    private fun openDashboardScreen() {
        GolHunterApp.applicationContext().loadData()
        finish()
        val intent = Intent(applicationContext, DashboardActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    private fun getUserDetailsFromFB() {
        if(areConditionsAccepted()) {
            val parameters = Bundle()
            parameters.putString("fields", "email,name")

            GraphRequest(
                    AccessToken.getCurrentAccessToken(),
                    "/me",
                    parameters,
                    HttpMethod.GET,
                    GraphRequest.Callback { response ->
                        try {
                            val email = response.jsonObject.getString("email")
                            val name = response.jsonObject.getString("name")
                            saveNewUser(email, name)
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
            ).executeAsync()
        } else {
            Toast.makeText(applicationContext, getString(R.string.error_accept_conditions), Toast.LENGTH_SHORT).show()
        }
    }

    private fun saveNewUser(email: String, name: String) {
        val user = ParseUser.getCurrentUser()
        user.username = name
        user.email = email
        user.put("balance", 100)
        user.saveInBackground({e ->
            if(e==null) {
                //GolHunterApp.applicationContext().prizeDataManager.postWelcomeGift()
                openDashboardScreen()
            }
        })
    }

    abstract inner class NoUnderlineSpan : ClickableSpan() {
        override fun updateDrawState(ds: TextPaint) {
            super.updateDrawState(ds)
            ds.isUnderlineText = false
        }
    }
}